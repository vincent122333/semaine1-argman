import static org.junit.Assert.*;

import org.junit.Test;

public class TestException {

    @Test(expected=RuntimeException.class)
    public void test() {
        new ExceptionTest().methodThrowsException();
    }

}
