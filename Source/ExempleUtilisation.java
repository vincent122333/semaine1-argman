
// Voici un exemple de "programme" qui utiliserait les fonctionnalités du ArgMan
public class ExempleUtilisation {

    // Vous devriez passer trois parametres au programme pour que l'exemple fonctionne:
    // -x entier [-y] [-z string]     (par exemple: -x 5 -z allo)
    /**
     * .
     * @param args la liste des arguments
     */
    public static void main(String[] args) {
        
        // Cette premiere ligne indique au ArgMan la configuration des parametres qu'il doit 
        // reconnaitre et valider (ie. le schéma).
        // En ce moment par contre, on initialize seulement un ArgManStub, qui ne fera rien d'utile 
        // avec les arguments qu'il recevra.
        ArgMan argman = new ArgManStub("x#,y$,z*?", args);
        
        // Tout le reste du travail du ArgMan se fait ensuite de manière invisible pour le programme
        // hôte. En fait, il n'a plus qu'à "questionner" le ArgMan pour accéder aux arguments.
        // L'interface du ArgMan permet au programme d'acceder à la valeur de chaque argument selon
        // un type spécifique.
        int entier = argman.getInt('x');
        boolean bool = argman.getBoolean('y');
        String str = argman.getString('z');

        // Le reste du programme fictif est simplement representé par un appel à "doSomething" ici.
        doSomething(entier, bool, str);
    }

    // Dans une situation réelle, ceci serait le reste du programme...
    private static void doSomething(int entier, boolean bool, String str) {
        cout("entier: " + entier);
        cout("booleen: " + bool);
        cout("chaine: " + str);
    }

    private static void cout(String message) {
        System.out.println(message);
    }
}
