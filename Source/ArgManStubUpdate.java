import java.util.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.swing.text.StyleConstants.CharacterConstants;

public final class ArgManStubUpdate implements ArgMan {

    
    private static final int MAX_VALUES_COUNT = 3;
    public static final List<Object> CONSTANTS = new ArrayList<>();
   
    
    private String[] params;
    private String[] args;
    private Map<String, Object> listeArgs;
    

    /**
     * Constructeur de ArgmanStub.
     * 
     * @param schema le schema
     */
    public ArgManStubUpdate(String schema) {
        System.out.println("Schema recu: " + schema);
        
        // Ceci est un squelette de "chargement" du schéma... Mais vous voudrez sûrement
        // l'améliorer un peu dans votre solution à vous. Entre-autres, il manque le support 
        // pour les paramètres optionnels ainsi qu'une meilleure gestion des erreurs.
        // TODO refactor this
        this.params = schema.split(",");
        for (String param : this.params) {
            char identifier = param.charAt(0);
            String type = "inconnu";
            switch (param.charAt(1)) {
                case '$':
                    type = "booleen";
                    break;
                case '#':
                    type = "entier";
                    break;
                case '*':
                    type = "chaine";
                    break;
                default:
                    break;
            }
            System.out.println(" -> Parametre " + identifier + " de type " + type);
        }
    }

    public ArgManStubUpdate(String schema, String[] args) {
        this(schema);
        loadArgs(args);
    }

    @Override
    public void loadArgs(String[] args) {
        CONSTANTS.add(true);
        CONSTANTS.add(0);
        CONSTANTS.add(" ");
        this.args = args;
        listeArgs = new HashMap<String, Object>();
        System.out.println("Arguments recus: " + Arrays.toString(args));

        // Après avoir reçu le schéma des paramètres, le ArgMan effectue les diverses actions  
        // requises pour pouvoir par la suite retourner les valeurs des arguments reçus.
        // (Ici par contre, c'est une classe "Stub", donc on ne fait rien de vraiment utile)
        // TODO refactor
        for (String arg : this.args) {
            if (arg.charAt(0) == '-') {
                char argId = arg.charAt(1); 
                boolean unknown = true;
                for (String param : this.params) {
                    if (argId == param.charAt(0)) {
                        unknown = false;
                        System.out.println(" -> Argument " + argId + " present");
                        int pos = Arrays.asList((this.args)).indexOf(arg);
                        String next = (this.args)[pos + 1];
                        if (next.charAt(0) == '-'){
                            listeArgs.put(Character.toString(argId), CONSTANTS.get(pos));
                        } else {
                            listeArgs.put(Character.toString(argId), next);
                        }
                    }
                }
                if (unknown) { // lancer exception a la place
                    System.out.println(" -> Argument " + argId + " inconnu");
                }
            } 
        }
        
    }

    @Override
    public int getInt(char paramName) {
        return 42;
    }

    @Override
    public boolean getBoolean(char paramName) {
        return true;
    }

    @Override
    public String getString(char paramName) {
        return "hello";
    }
    
    public static void main(String[] args) {
        final String schema = "t$,e#?,s*";
        final String sampleArgs = "-t -e 5 -s hello";
        ArgMan argman = new ArgManStubUpdate(schema);
        argman.loadArgs(sampleArgs.split(" "));
    }
}
